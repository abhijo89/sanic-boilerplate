import json
from typing import Dict

from utils.utils_constants import Status
from sanic.views import HTTPMethodView
from sanic.log import logger
from sanic import response


class RestResponse(object):
    @classmethod
    def get(cls, status: str, _type: str, message: str, result: Dict, status_code: int=200,):
        data = {'type': _type, 'status': status, 'message': message}
        if result is not None:
            data['data'] = result
        logger.debug(json.dumps(data, indent=4))
        return response.json(data, status_code)


class RestBase(HTTPMethodView):

    def process_get(self, request):
        return Status.SUCCESS.value, {}

    def get(self, request):
        data_dict = {}
        try:
            status, data_dict = self.process_get(request)
        except Exception:
            status = Status.FAILURE.value

        return RestResponse.get(status, None, '', data_dict)

    def post(self, request):

        data_dict = {}
        try:
            status, data_dict = self.process_get(request)
        except Exception:
            status = Status.FAILURE.value

        return RestResponse.get(status, None, '', data_dict)

    def put(self, request):

        data_dict = {}
        try:
            status, data_dict = self.process_get(request)
        except Exception:
            status = Status.FAILURE.value

        return RestResponse.get(status, None, '', data_dict)

    def patch(self, request):

        data_dict = {}
        try:
            status, data_dict = self.process_get(request)
        except Exception:
            status = Status.FAILURE.value

        return RestResponse.get(status, None, '', data_dict)

    def delete(self, request):

        data_dict = {}
        try:
            status, data_dict = self.process_get(request)
        except Exception:
            status = Status.FAILURE.value

        return RestResponse.get(status, None, '', data_dict)
