from setuptools import setup

setup(
    name='sanic-boilerplate',
    version='2019.10',
    packages=['bp', 'utils', 'views', 'views.module1', 'views.module2'],
    url='',
    license='',
    author='Abhilash Joseph C',
    author_email='abhilash@softlinkweb.com',
    description=''
)
